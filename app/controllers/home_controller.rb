class HomeController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    redirect_to "http://localhost:8080/"
  end

  def events
    date = Time.parse(params[:date])
    #start_week = (date - 1.days)
    #end_week = (date + 8.days)
    start_week = date.beginning_of_week(:sunday)
    end_week = (start_week + 1.days).end_of_week
    service_id = params[:service]
    service = Service.where(id: service_id).first
    events = Event.where(service_id: service_id, date: start_week..end_week).includes(:user)
    evnts = []
    events.each do |event|
      evnts << {id: event.id, title: event.user.name, start: event.date, backgroundColor: event.user.color, borderColor: event.user.color, classNames: "slot-user-#{event.user.id}"}
    end
    if service.present?
      #service.set_user_hours(start_week)
      service.service_events.each do |service_event|
        current_day = start_week + (service_event.date.wday).days
        start_at = Time.parse(service_event.date.strftime("%H:%M UTC"), current_day)
        end_at = start_at + 1.hours
        if service_event.date_end
          end_day = start_week + (service_event.date_end.wday).days
          end_day = end_day + 7.days if service_event.date.wday == 6 && service_event.date_end.wday == 0
          end_at = Time.parse(service_event.date_end.strftime("%H:%M UTC"), end_day)
        end
        evnts << {start: start_at, end: end_at, display: "background", color: "red"}
      end
    end

    render(json: evnts.to_json)
  end

  def create
    got_params = params.require(:event).permit(:service_id, :user_id, :date)
    @event = Event.where(got_params).first_or_initialize
    if !@event.id && @event.save
      render json: @event.to_json
    else
      render json: {errors: @event.errors}
    end
  end

  def destroy
    event_id = params[:id]
    event = Event.where(id: event_id).first
    if event && event.destroy
      render json: {}
    else
      render json: {errors: {}}
    end
  end

  def update
    event = Event.where(id: params[:id]).first
    already_set = Event.where(user_id: event.user_id, service_id: event.service_id, date: params[:date]).where.not(id: event.id).first
    if event && !already_set && event.update(date: params[:date])
      render json: {} if !already_set
    else
      render json: {errors: {}}
    end
  end

end
