class Service < ApplicationRecord
  has_many :service_events
  has_many :events

  def set_user_hours(start_week = Time.zone.now.beginning_of_week(:sunday))
    sorted_events = self.service_events.select("id, date_end, date").sort_by{|x| (x.date_end || (x.date + 1.hours)) -  x.date}.reverse
    users = User.order("hours_per_week asc")

    events_hash = {}
    sorted_events.each do |sorted_event|
      events_hash["#{sorted_event.id}"] = {hours: ((sorted_event.date_end - sorted_event.date) / 3600).to_i, date_end: sorted_event.date_end, date: sorted_event.date, completed: false}
    end

    users_hash = {}
    users.each do |user|
      users_hash["#{user.id}"] = {hours: user.hours_per_week, events: [], completed: false}
    end

    sorted_evs = events_hash.sort_by{|key, value| value[:hours]}.reverse.to_h
    sorted_evs.each do |k,v|
      users_hash.sort_by{|key, value| value[:hours]}.reverse.to_h.each do |k2,v2|
        events = []
        v2[:completed] = true if ((v2[:hours] - v[:hours].to_i) < 0)
        if v[:completed] || v2[:completed]
          puts "Skipping User"
          next
        end
        v2[:hours] = v2[:hours] - v[:hours].to_i
        v[:hours].to_i.times do |index|
          current_day = start_week + (v[:date].wday).days
          start_at = Time.parse(v[:date].strftime("%H:%M UTC"), current_day) + index.hours
          end_at = start_at + 1.hours
          #v2[:events] << {date: start_at, date_end: end_at, service_id: self.id, user_id: k2}
          Event.where({date: start_at, date_end: end_at, service_id: self.id, user_id: k2}).first_or_create
        end
        v[:completed] = true
      end
    end
    return users_hash
  end


end
