class Api::ServicesController < ApplicationController
    skip_before_action :verify_authenticity_token
  def index
    render json: Service.all
  end

  def set_schedule
    service = Service.where(id: params[:id]).first
    if service
      date = Time.parse(params[:data][:date]).beginning_of_week(:sunday)
      service.set_user_hours(date)
    end
    render json: {}
  end
end
