class AddDatendToServiceEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :service_events, :date_end, :datetime
  end
end
