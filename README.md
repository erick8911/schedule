Steps

Download the backend project

    git clone git@gitlab.com:erick8911/schedule.git


Run backend setup

    cd schedule
    rails db:create
    rails db:migrate
    rails db:seed

Start Server

    rails s

Download the frontend

        git clone https://gitlab.com/erick8911/schedule-front

Run Frontend Setup

    cd schedule-front
    yarn install
    yarn dev
