class CreateServiceEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :service_events do |t|
      t.datetime :date
      t.integer :service_id

      t.timestamps
    end
  end
end
