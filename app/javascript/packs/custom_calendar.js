import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

import interactionPlugin, {Draggable} from '@fullcalendar/interaction'
import bootstrapPlugin from '@fullcalendar/bootstrap';
import axios from "axios";

var calendar;
document.addEventListener('turbolinks:load', () => {

  var containerEl = document.getElementById('external-events');
  new Draggable(containerEl, {
    itemSelector: '.fc-event',
  });

  var myModalEl = document.getElementById('exampleModal')
  var myModal = new bootstrap.Modal(myModalEl, {})
  var calendarEl = document.querySelector("#calendar")
  if (calendarEl != undefined){
    var serviceSelector = document.getElementById('service-selector');
    calendar = new Calendar(calendarEl, {
      plugins: [ dayGridPlugin, timeGridPlugin, listPlugin, bootstrapPlugin, interactionPlugin],
      editable: true,
      eventDurationEditable: false,
      slotLaneDidMount: function(arg){
        //console.log(arg)
      },
      //themeSystem: "bootstrap",
      initialView: 'timeGridWeek',
      headerToolbar: {
          left: null,
          center: 'title',
          right: 'prev,next today',
      },
      views: {
        timeGrid:{
          slotDuration: "00:60:00"
        }
      },
      eventsSet: function(events){
        let draggableUser = document.querySelectorAll(".draggable-user")
        if (draggableUser){
          draggableUser.forEach((item, i) => {
            let slctr = ".slot-user-" + item.getAttribute("data-user-id")
            let userSlot = document.querySelectorAll(slctr)
            if (userSlot){
              item.querySelector("span").innerText = userSlot.length
            }
          });
        }

      },
      eventClick: function(info){
        myModal.show(info)
      },
      eventDragStart: function(info){
        var eventDestroyEl = document.querySelector("#event-destroy")
        eventDestroyEl.classList.add("destroyable");
      },
      eventDragStop: function(info){
        console.log(info)
        var jsEvent = info.jsEvent
        var eventDestroyEl = document.querySelector("#event-destroy")
        var x1 = eventDestroyEl.offsetLeft;
        var x2 = eventDestroyEl.offsetLeft + eventDestroyEl.offsetWidth;
        var y1 = eventDestroyEl.offsetTop;
        var y2 = eventDestroyEl.offsetTop + eventDestroyEl.offsetHeight;
        if (jsEvent.pageX >= x1 && jsEvent.pageX <= x2 && jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
          axios.delete('http://localhost:3000/events', {data: {id: info.event.id}}).then(function(response){
            if (response.data?.errors){
              //Nothing to do
            }else{
              info.event.remove();
            }

          })
        }
        var eventDestroyEl = document.querySelector("#event-destroy")
        eventDestroyEl.classList.remove("destroyable");
      },
      droppable: true,
      drop: function(info){
      },
      eventDrop: function(info){
        var url = 'http://localhost:3000/events/' + info.event.id
        axios.put(url, { date: info.event.start }).then(function(response){
          if (response.data?.errors){
            info.revert()
          }
        })
      },
      eventReceive: function(info){
        var serviceValue = serviceSelector.options[serviceSelector.selectedIndex].value;
        var userId = info.draggedEl.getAttribute("data-user-id");
        var url = 'http://localhost:3000/events'
        axios.post(url,{
          event: {
            service_id: serviceValue,
            user_id: userId,
            date: info.event.start
          }
        }).then(function (response) {
          if (response.data?.errors){
            info.revert()
          }
        })
      },
      eventSources: [{
        events: function(info, successCallback, failureCallback){
          var serviceValue = serviceSelector.options[serviceSelector.selectedIndex].value;
          var url = 'http://localhost:3000/events?service=' + serviceValue + "&date=" + info.startStr
          axios.get(url).then(function (response){
            successCallback(response.data)
          })
        }
      }]
    });
    calendar.render();
    myModalEl.addEventListener('shown.bs.modal', function (event) {
      var calendarEvent = event.relatedTarget.event
      document.querySelector(".modal-title").innerText = calendarEvent.title + " " + calendarEvent.start
    })
    serviceSelector.addEventListener("change", function(){
      calendar.removeAllEvents();
      calendar.refetchEvents()
    });
  }else{
    console.log("Not found!")
  }
})
