class AddHoursToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :hours_per_week, :integer
    add_column :events, :end_at, :datetime
  end
end
