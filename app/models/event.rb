class Event < ApplicationRecord
  validates :date, presence: true
  belongs_to :user
  belongs_to :service

  alias_attribute :date_end, :end_at

  before_save :set_end_date
  def set_end_date
    if !self.date_end.present?
      self.date_end = self.date + 1.hours
    end
    true
  end
end
