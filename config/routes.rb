Rails.application.routes.draw do
  root to: "home#index"
  get    "events", to: "home#events"
  post   "events", to: "home#create"
  delete "events", to: "home#destroy"
  put    "events/:id", to: "home#update"

  resources :services do
    get "events", to: "services#events", on: :member
  end

  namespace :api do
    resources :services do
      resources :service_events
      post "set-schedule", to: "services#set_schedule", on: :member
    end
    get "users", to: "users#index"
  end

end
