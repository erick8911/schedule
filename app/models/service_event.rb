class ServiceEvent < ApplicationRecord

  before_save :set_end_date
  def set_end_date
    if !self.date_end.present?
      self.date_end = self.date + 1.hours
    end
    true
  end

end
