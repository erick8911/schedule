if !Service.exists?
  puts "Creating Services"
  5.times do |index|
    Service.create({name: "Service #{index}"})
  end

end

if !ServiceEvent.exists?
  puts "Creating Service Events"
  Service.all.each do |service|
    evnts = []
    date = Time.zone.now.beginning_of_week
    end_week = date.end_of_week
    begin
      if date.wday == 0 || date.wday == 6
        evnts << {date: (date + 10.hours), date_end: (date + 24.hours)}
      else
        evnts << {date: (date + 19.hours), date_end: (date + 24.hours)}
      end
      date = date + 1.days
    end while date <= end_week
    service.service_events.create(evnts)
  end
end

if !User.exists?
  colors = ["#007bff", "#6c757d", "#28a745", "#dc3545", "#ffc107", "#17a2b8", "#343a40"]
  puts "Creating Users"
  User.create([
    {name: "Ernesto", color: colors[0], hours_per_week: 17},
    {name: "Barbara", color: colors[1], hours_per_week: 19},
    {name: "Benjamin", color: colors[2], hours_per_week: 16},
  ])
end


#if !Event.exists?
#  puts "Creating Events"
#  week_start = Time.zone.now.beginning_of_week
#  week_end = week_start.end_of_week
#  user = User.first
#  service = Service.first
#  3.times do |index|
#    puts week_start
#    date_start = week_start + (16 + index).hours
#    puts date_start
#    Event.create({user_id: user.id, service_id: service.id, date: date_start.to_time})
#  end
#end
