class Api::ServiceEventsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    service_events = ServiceEvent.where(service_id: params[:service_id]).order("date ASC")
    events = []
    service_events.each do |service_event|
      current_day = Time.zone.now.beginning_of_week(:sunday) + (service_event.date.wday).days
      start_at = Time.parse(service_event.date.strftime("%H:%M UTC"), current_day)
      end_at = start_at + 1.hours
      if service_event.date_end
        end_day = Time.zone.now.beginning_of_week(:sunday) + (service_event.date_end.wday).days
        end_day = end_day + 7.days if service_event.date.wday == 6 && service_event.date_end.wday == 0
        end_at = Time.parse(service_event.date_end.strftime("%H:%M UTC"), end_day)
      end
      events << {id: service_event.id, title: "Service", start: start_at, end: end_at}
    end
    render json: events.to_json
  end

  def create
    got_params = params.require(:event).permit(:service_id, :date)
    @event = ServiceEvent.where(got_params).first_or_initialize
    if !@event.id && @event.save
      render json: {}
    else
      render json: {errors: @event.errors}
    end
  end

  def update
    got_params = params.require(:service_event).permit(:date, :date_end)
    service_event = ServiceEvent.where(id: params[:id]).first
    service_event.update(got_params)
  end

  def destroy
    service_event = ServiceEvent.where(id: params[:id]).first
    service_event.destroy
  end

end
